<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'phalcon',
        'charset'     => 'utf8mb4',
    ),
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => '/showoff/',
    ),
    'custom_routes' => array(
        array(
            'route' => '/api/user/:action[/]{0,1}([0-9]*)[/]{0,1}',
            'param' => array(
                'controller' => 'api',
                'is_routed'  => 'true',
                'action'     => 1,
                'userid'     => 2
            ),
            'type' => array("GET", "PUT", "POST"),
            'host' => $_SERVER['SERVER_NAME']
        ),
        array(
            'route' => '/404[/]{0,1}',
            'param' => array(
                'controller' => 'index',
                'action'     => 'route404',
                'is_routed'  => 'true'
            ),
            'type' => array("GET"),
            'host' => $_SERVER['SERVER_NAME']
        )
    )
));
