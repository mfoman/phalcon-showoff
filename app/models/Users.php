<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;

class Users extends Model
{
    public $id;

    public $name;

    public $email;

    public function validation()
    {

        $this->validate(
            new EmailValidator(
                array(
                    'field'   => 'email',
                    'message' => '<h3>Det skal være en rigtig email!</h3>'
                )
            )
        );

        $this->validate(
            new Uniqueness(
                array(
                    'field'   => 'name',
                    'message' => '<h3>Brugernavn skal være unikt!</h3>'
                )
            )
        );

        $this->validate(
            new Uniqueness(
                array(
                    'field'   => 'email',
                    'message' => '<h3>Email skal være unik!</h3>'
                )
            )
        );

        return $this->validationHasFailed() != true;

    }
}
