<?php

class SignupController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function registerAction()
    {

        $user = new Users();

        $success = $user->save($this->request->getPost(), ['name', 'email']);

        if ($success) {
            echo "<h1>Tak!</h1><h2>Nyd indholdet på siden!</h2>";
        } else {
            foreach ($user->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
            }

            echo $this->tag->linkTo("signup", "Gå tilbage til tilmelding!");
        }

        //$this->view->disable();

    }

}
