<?php

class ApiController extends ControllerBase
{

    public function indexAction()
    {



    }

    /**
     * Check if request is routed through custom routes defined in the configs.
     * This denies the default route machine from accessing the controller.
     */
    private function isRouted() {

        return null !== $this->dispatcher->getParam('is_routed') ? true : false;

    }

    /**
     * This checks if the request isRouted and then take measures to prevent access.
     */
    private function checkRouted() {

        if (!$this->isRouted()) {
            $this->response->redirect('404');
            $this->view->disable();
            return false;
        }
        return true;

    }

    private function checkType($type)
    {

        switch ($type) {
            case 'get':
                if ($this->request->isGet()) {
                    return true;
                }
                break;

            case 'post':
                if ($this->request->isPost()) {
                    return true;
                }
                break;

            case 'put':
                if ($this->request->isPut()) {
                    return true;
                }
                break;
        }
        $this->view->disable();
        return false;

    }

    /**
     * This checks if the request isn't through Ajax and take measures to prevent it.
     */
    private function checkAjax()
    {

        if ($this->request->isAjax() != true) {
            echo "Should be a AJAX request\n";
            $this->view->disable();
            return false;
        }
        return true;

    }

    /**
     * A method for creating a response.
     */
    private function returnMessage($data, $status = ['code' => 200, 'message' => 'OK'])
    {

        $response = new \Phalcon\Http\Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setStatusCode($status['code'], $status['message']);
        $response->setJsonContent($data);

        return $response;

    }

    /**
     * This is the core get method of the api.
     */
    public function getAction()
    {
        global $application;

        $this->checkRouted();

        if (!$this->checkType('get')) {

            return $this->returnMessage(array(
                'status'  => 405,
                'message' => 'Should be a GET request'
            ), array(
                'code'    => 405,
                'message' => 'Wrong method used'
            ));

        }

        // $this->checkAjax(); 422

        $phpl = "SELECT * FROM Users WHERE id = :id:";
        $users = $application->modelsManager->executeQuery(
            $phpl,
            array(
                'id' => $this->dispatcher->getParam('userid', 'int')
            )
        )->getFirst();

        if ($users == false) {

            return $this->returnMessage(array(
                'status'  => 404,
                'message' => 'Not Found'
            ), array(
                'code'    => 404,
                'message' => 'Not Found'
            ));

        } else {

            return $this->returnMessage(array(
                'status'  => 200,
                'data'    => array(
                    'id'    => $users->id,
                    'name'  => $users->name,
                    'email' => $users->email
                )
            ), array(
                'code'    => 202,
                'message' => 'OK'
            ));

        }

    }

    /**
     * The core api method for setting up new rows.
     */
    public function postAction()
    {

        $this->checkRouted();

        if (!$this->checkType('post')) {

            return $this->returnMessage(array(
                'status'  => 405,
                'message' => 'Should be a POST request'
            ), array(
                'code'    => 405,
                'message' => 'Wrong method used'
            ));

        }

        // $this->checkAjax();

        $user = new Users();

        $success = $user->create($this->request->getPost(), ['name', 'email']);

        if ($success) {

            return $this->returnMessage(array(
                'status'  => 200,
                'message' => 'OK'
            ), array(
                'code'    => 200,
                'message' => 'OK'
            ));

        } else {
            $errors = array();

            foreach ($user->getMessages() as $message) {
                $errors[] = $message->getMessage() . "<br/>";
            }

            return $this->returnMessage(array(
                'status'  => 422,
                'message' => $errors
            ), array(
                'code'    => 422,
                'message' => 'Unprocessable Entity'
            ));

        }

    }

    /**
     * The api method for updateing existing rows.
     */
    public function putAction()
    {

        print_r($this->request->getPut());

        $this->checkRouted();

        if (!$this->checkType('put')) {

            return $this->returnMessage(array(
                'status'  => 405,
                'message' => 'Should be a PUT request'
            ), array(
                'code'    => 405,
                'message' => 'Wrong method used'
            ));

        }

        // $this->checkAjax();
        $id = $this->request->getPut("id", "int");

        $user = Users::findFirstById($id);

        if (!$user) {

            return $this->returnMessage(array(
                'status'  => 404,
                'message' => 'User was not found'
            ), array(
                'code'    => 404,
                'message' => 'Not Found'
            ));

        }

        $success = $user->update($this->request->getPut(), ['name', 'email']);

        if ($success) {

            return $this->returnMessage(array(
                'status'  => 200,
                'message' => 'OK'
            ), array(
                'code'    => 200,
                'message' => 'OK'
            ));

        } else {
            $errors = array();

            foreach ($user->getMessages() as $message) {
                $errors[] = $message->getMessage() . "<br/>";
            }

            return $this->returnMessage(array(
                'status'  => 422,
                'message' => $errors
            ), array(
                'code'    => 422,
                'message' => 'Unprocessable Entity'
            ));

        }

    }

}
